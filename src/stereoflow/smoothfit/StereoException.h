#ifndef STEREOEXCEPTION_H
#define STEREOEXCEPTION_H

#include <string>

class StereoException {
public:
    StereoException(const std::string functionName = "", const std::string message = "")
        : functionName_(functionName), message_(message) {}
    
    std::string functionName() const { return functionName_; }
    std::string message() const { return message_; }
    
private:
    std::string functionName_;
    std::string message_;
};

#endif
