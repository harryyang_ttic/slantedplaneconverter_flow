#include "Segment.h"
#include <algorithm>
#include "StereoException.h"

void Segment::clear() {
    centerX_ = -1;
    centerY_ = -1;
    interiorPoints_.clear();
    neighborSegmentIndices_.clear();
    boundaryIndices_.clear();
    segmentPixelXs_.clear();
    segmentPixelYs_.clear();
}

SupportPoint Segment::interiorPoint(const int index) const {
    if (index < 0 || index >= static_cast<int>(interiorPoints_.size())) {
        throw StereoException("Segment::interiorPoint", "index is not valid");
    }
    
    return interiorPoints_[index];
}

int Segment::neighborIndex(const int index) const {
    if (index < 0 || index >= static_cast<int>(neighborSegmentIndices_.size())) {
        throw StereoException("Segment::neighborIndex", "index is not valid");
    }
    
    return neighborSegmentIndices_[index];
}

int Segment::boundaryIndex(const int index) const {
    if (index < 0 || index >= static_cast<int>(boundaryIndices_.size())) {
        throw StereoException("Segment::interiorPoint", "index is not valid");
    }
    
    return boundaryIndices_[index];
}

int Segment::segmentPixelX(const int index) const {
    if (index < 0 || index >= static_cast<int>(segmentPixelXs_.size())) {
        throw StereoException("Segment::segmentPixelX", "index is not valid");
    }
    
    return segmentPixelXs_[index];
}

int Segment::segmentPixelY(const int index) const {
    if (index < 0 || index >= static_cast<int>(segmentPixelYs_.size())) {
        throw StereoException("Segment::segmentPixelY", "index is not valid");
    }
    
    return segmentPixelYs_[index];
}

void Segment::setInteriorPointDisparity(const int pointIndex, const double disparity) {
    if (pointIndex < 0 || pointIndex >= static_cast<int>(interiorPoints_.size())) {
        throw StereoException("Segment::setInteriorPointDisparity", "index is not valid");
    }
    
    int x = interiorPoints_[pointIndex].x();
    int y = interiorPoints_[pointIndex].y();
    interiorPoints_[pointIndex].clear();
    interiorPoints_[pointIndex].set(x, y);
    interiorPoints_[pointIndex].appendDisparity(disparity);
}


SegmentBoundary::SegmentBoundary() {
    segmentIndices_[0] = -1;
    segmentIndices_[1] = -1;
}

SegmentBoundary::SegmentBoundary(const int firstSegmentIndex, const int secondSegmentIndex) {
    set(firstSegmentIndex, secondSegmentIndex);
}

void SegmentBoundary::set(const int firstSegmentIndex, const int secondSegmentIndex)
{
    if (firstSegmentIndex < 0 || secondSegmentIndex < 0) {
        throw StereoException("SegmentBoundary::set", "segment index is negaive");
    }
    if (firstSegmentIndex == secondSegmentIndex) {
        throw StereoException("SegmentBoundary::set", "two segment indices are the same");
    }
    
    if (firstSegmentIndex < secondSegmentIndex) {
        segmentIndices_[0] = firstSegmentIndex;
        segmentIndices_[1] = secondSegmentIndex;
    } else {
        segmentIndices_[0] = secondSegmentIndex;
        segmentIndices_[1] = firstSegmentIndex;
    }
}

int SegmentBoundary::segmentIndex(const int index) const {
    if (index < 0 || index >= 2) {
        throw StereoException("SegmentBoundary::segmentIndex", "index must be 0 or 1");
    }
    
    return segmentIndices_[index];
}

bool SegmentBoundary::consistOf(const int firstSegmentIndex, const int secondSegmentIndex) const {
    if ((firstSegmentIndex == segmentIndices_[0] && secondSegmentIndex == segmentIndices_[1])
        || (firstSegmentIndex == segmentIndices_[1] && secondSegmentIndex == segmentIndices_[0]))
    {
        return true;
    }
    return false;
}

int SegmentBoundary::include(const int segmentIndex) const {
    if (segmentIndex == segmentIndices_[0]) return 0;
    else if (segmentIndex == segmentIndices_[1]) return 1;
    else return -1;
}

SupportPoint SegmentBoundary::boundaryPoint(const int index) const {
    if (index < 0 || index >= static_cast<int>(boundaryPoints_.size())) {
        throw StereoException("SegmentBoundary::boundaryPoint", "index is not valid");
    }
    
    return boundaryPoints_[index];
}

double SegmentBoundary::boundaryPixelX(const int index) const {
    if (index < 0 || index >= static_cast<int>(boundaryPixelXs_.size())) {
        StereoException("SegmentBoundary::boundaryPixelX", "index is not valid");
    }
	
    return boundaryPixelXs_[index];
}

double SegmentBoundary::boundaryPixelY(const int index) const {
    if (index < 0 || index >= static_cast<int>(boundaryPixelYs_.size())) {
        StereoException("SegmentBoundary::boundaryPixelY", "index is not valid");
    }
    
    return boundaryPixelYs_[index];
}

void SegmentBoundary::setBoundaryPointDisparity(const int pointIndex, const double disparity) {
    if (pointIndex < 0 || pointIndex >= static_cast<int>(boundaryPoints_.size())) {
        throw StereoException("SegmentBoundary::setBoundaryPointDisparity", "index is not valid");
    }
    
    int x = boundaryPoints_[pointIndex].x();
    int y = boundaryPoints_[pointIndex].y();
    boundaryPoints_[pointIndex].clear();
    boundaryPoints_[pointIndex].set(x, y);
    boundaryPoints_[pointIndex].appendDisparity(disparity);
}


SegmentJunction::SegmentJunction() : x_(-1), y_(-1) {
    segmentIndices_[0] = -1;
    segmentIndices_[1] = -1;
    segmentIndices_[2] = -1;
    boundaryIndices_[0] = -1;
    boundaryIndices_[1] = -1;
    boundaryIndices_[2] = -1;
}

SegmentJunction::SegmentJunction(const int x, const int y) : x_(x), y_(y) {
    segmentIndices_[0] = -1;
    segmentIndices_[1] = -1;
    segmentIndices_[2] = -1;
    boundaryIndices_[0] = -1;
    boundaryIndices_[1] = -1;
    boundaryIndices_[2] = -1;
}

void SegmentJunction::setPosition(const int x, const int y) {
    x_ = x;
    y_ = y;
}

void SegmentJunction::setSegmentIndices(const std::vector<int>& threeSegmentIndices) {
    if (threeSegmentIndices.size() != 3) {
        throw StereoException("SegmentJunction::setSegmentIndices", "the number of segment indices is not 3");
    }
    if (threeSegmentIndices[0] < 0 || threeSegmentIndices[1] < 0 || threeSegmentIndices[2] < 0) {
        throw StereoException("SegmentJunction::setSegmentIndices", "segment index is negative");
    }
    if (threeSegmentIndices[0] == threeSegmentIndices[1]
        || threeSegmentIndices[0] == threeSegmentIndices[2]
        || threeSegmentIndices[1] == threeSegmentIndices[2])
    {
        throw StereoException("SegmentJunction::setSegmentIndices", "two segment indices are the same");
    }
    
	std::vector<int> sortIndices = threeSegmentIndices;
	std::sort(sortIndices.begin(), sortIndices.end());
    
    segmentIndices_[0] = sortIndices[0];
    segmentIndices_[1] = sortIndices[1];
    segmentIndices_[2] = sortIndices[2];
}

void SegmentJunction::setBoundaryIndices(const std::vector<int>& threeBoundaryIndices) {
    if (threeBoundaryIndices.size() != 3) {
        throw StereoException("SegmentJunction::setBoundaryIndices", "the number of boundary indices is not 3");
    }
    if (threeBoundaryIndices[0] < 0 || threeBoundaryIndices[1] < 0 || threeBoundaryIndices[2] < 0) {
        throw StereoException("SegmentJunction::setBoundaryIndices", "boundary index is negative");
    }
    if (threeBoundaryIndices[0] == threeBoundaryIndices[1]
        || threeBoundaryIndices[0] == threeBoundaryIndices[2]
        || threeBoundaryIndices[1] == threeBoundaryIndices[2])
    {
        throw StereoException("SegmentJunction::setBoundaryIndices", "two boundary indices are the same");
    }
    
    boundaryIndices_[0] = threeBoundaryIndices[0];
    boundaryIndices_[1] = threeBoundaryIndices[1];
    boundaryIndices_[2] = threeBoundaryIndices[2];
    
}

int SegmentJunction::segmentIndex(const int index) const {
    if (index < 0 || index >= 3) {
        throw StereoException("SegmentJunction::segmentIndex", "index must be between 0 and 2");
    }
    
    return segmentIndices_[index];
}

bool SegmentJunction::consistOf(const std::vector<int>& threeSegmentIndices) const {
    if (threeSegmentIndices.size() != 3) {
        throw StereoException("SegmentJunction::consistOf", "the number of segment indices is not 3");
    }
	
	std::vector<int> sortIndices = threeSegmentIndices;
	std::sort(sortIndices.begin(), sortIndices.end());
    
    if (sortIndices[0] == segmentIndices_[0] && sortIndices[1] == segmentIndices_[1] && sortIndices[2] == segmentIndices_[2]) {
        return true;
    }
    return false;
}

int SegmentJunction::boundaryIndex(const int index) const {
    if (index < 0 || index >= 3) {
        throw StereoException("SegmentJunction::boundaryIndex", "index must be between 0 and 2");
    }
    
    return boundaryIndices_[index];
}

SupportPoint SegmentJunction::junctionPoint(const int index) const {
    if (index < 0 || index >= static_cast<int>(junctionPoints_.size())) {
        throw StereoException("SegmentJunction::junctionPoint", "index is not valid");
    }
    
    return junctionPoints_[index];
}

void SegmentJunction::setJunctionPointDisparity(const int pointIndex, const double disparity) {
    if (pointIndex < 0 || pointIndex >= static_cast<int>(junctionPoints_.size())) {
        throw StereoException("SegmentJunction::setJunctionPointDisparity", "index is not valid");
    }
    
    int x = junctionPoints_[pointIndex].x();
    int y = junctionPoints_[pointIndex].y();
    junctionPoints_[pointIndex].clear();
    junctionPoints_[pointIndex].set(x, y);
    junctionPoints_[pointIndex].appendDisparity(disparity);
}

SegmentCrossJunction::SegmentCrossJunction() : x_(-1), y_(-1) {
    segmentIndices_[0] = -1;
    segmentIndices_[1] = -1;
    segmentIndices_[2] = -1;
    segmentIndices_[3] = -1;
    boundaryIndices_[0] = -1;
    boundaryIndices_[1] = -1;
    boundaryIndices_[2] = -1;
    boundaryIndices_[3] = -1;
}

SegmentCrossJunction::SegmentCrossJunction(const int x, const int y) : x_(x), y_(y) {
    segmentIndices_[0] = -1;
    segmentIndices_[1] = -1;
    segmentIndices_[2] = -1;
    segmentIndices_[3] = -1;
    boundaryIndices_[0] = -1;
    boundaryIndices_[1] = -1;
    boundaryIndices_[2] = -1;
    boundaryIndices_[3] = -1;
}

void SegmentCrossJunction::setPosition(const int x, const int y) {
    x_ = x;
    y_ = y;
}

void SegmentCrossJunction::setSegmentIndices(const std::vector<int>& fourSegmentIndices) {
    if (fourSegmentIndices.size() != 4) {
        throw StereoException("SegmentCrossJunction::setSegmentIndices", "the number of segment indices is not 4");
    }
    if (fourSegmentIndices[0] < 0 || fourSegmentIndices[1] < 0 || fourSegmentIndices[2] < 0 || fourSegmentIndices[3] < 0) {
        throw StereoException("SegmentCrossJunction::setSegmentIndices", "segment index is negative");
    }
    if (fourSegmentIndices[0] == fourSegmentIndices[1]
        || fourSegmentIndices[0] == fourSegmentIndices[2]
        || fourSegmentIndices[0] == fourSegmentIndices[3]
        || fourSegmentIndices[1] == fourSegmentIndices[2]
        || fourSegmentIndices[1] == fourSegmentIndices[3]
        || fourSegmentIndices[2] == fourSegmentIndices[3])
    {
        throw StereoException("SegmentCrossJunction::setSegmentIndices", "two segment indices are the same");
    }
    
    segmentIndices_[0] = fourSegmentIndices[0];
    segmentIndices_[1] = fourSegmentIndices[1];
    segmentIndices_[2] = fourSegmentIndices[2];
    segmentIndices_[3] = fourSegmentIndices[3];
}

void SegmentCrossJunction::setBoundaryIndices(const std::vector<int>& fourBoundaryIndices, const std::vector<bool>& fourBoundaryDirections) {
    if (fourBoundaryIndices.size() != 4 || fourBoundaryDirections.size() != 4) {
        throw StereoException("SegmentCrossJunction::setBoundaryIndices", "the number of boundary indices is not 4");
    }
    if (fourBoundaryIndices[0] < 0 || fourBoundaryIndices[1] < 0 || fourBoundaryIndices[2] < 0 || fourBoundaryIndices[3] < 0) {
        throw StereoException("SegmentCrossJunction::setBoundaryIndices", "boundary index is negative");
    }
    if (fourBoundaryIndices[0] == fourBoundaryIndices[1]
        || fourBoundaryIndices[0] == fourBoundaryIndices[2]
        || fourBoundaryIndices[0] == fourBoundaryIndices[3]
        || fourBoundaryIndices[1] == fourBoundaryIndices[2]
        || fourBoundaryIndices[1] == fourBoundaryIndices[3]
        || fourBoundaryIndices[2] == fourBoundaryIndices[3])
    {
        throw StereoException("SegmentCrossJunction::setBoundaryIndices", "two boundary indices are the same");
    }
    
    boundaryIndices_[0] = fourBoundaryIndices[0];
    boundaryIndices_[1] = fourBoundaryIndices[1];
    boundaryIndices_[2] = fourBoundaryIndices[2];
    boundaryIndices_[3] = fourBoundaryIndices[3];
    
    boundaryDirections_[0] = fourBoundaryDirections[0];
    boundaryDirections_[1] = fourBoundaryDirections[1];
    boundaryDirections_[2] = fourBoundaryDirections[2];
    boundaryDirections_[3] = fourBoundaryDirections[3];
}

int SegmentCrossJunction::segmentIndex(const int index) const {
    if (index < 0 || index >= 4) {
        throw StereoException("SegmentCrossJunction::segmentIndex", "index must be between 0 and 3");
    }
    
    return segmentIndices_[index];
}

bool SegmentCrossJunction::consistOf(const std::vector<int>& fourSegmentIndices) const {
    if (fourSegmentIndices.size() != 4) {
        throw StereoException("SegmentCrossJunction::consistOf", "the number of segment indices is not 4");
    }
    
	std::vector<int> sortIndices = fourSegmentIndices;
	std::sort(sortIndices.begin(), sortIndices.end());
    
    if (sortIndices[0] == segmentIndices_[0] && sortIndices[1] == segmentIndices_[1]
        && sortIndices[2] == segmentIndices_[2] && sortIndices[3] == segmentIndices_[3]) {
        return true;
    }
    return false;
}

int SegmentCrossJunction::boundaryIndex(const int index) const {
    if (index < 0 || index >= 4) {
        throw StereoException("SegmentCrossJunction::boundaryIndex", "index must be between 0 and 3");
    }
    
    return boundaryIndices_[index];
}

bool SegmentCrossJunction::boundaryDirection(const int index) const {
    if (index < 0 || index >= 4) {
        throw StereoException("SegmentCrossJunction::boundaryDirection", "index must be between 0 and 3");
    }
    
    return boundaryDirections_[index];
}

SupportPoint SegmentCrossJunction::junctionPoint(const int index) const {
    if (index < 0 || index >= static_cast<int>(junctionPoints_.size())) {
        throw StereoException("SegmentCrossJunction::junctionPoint", "index is not valid");
    }
    
    return junctionPoints_[index];
}

void SegmentCrossJunction::setJunctionPointDisparity(const int pointIndex, const double disparity) {
    if (pointIndex < 0 || pointIndex >= static_cast<int>(junctionPoints_.size())) {
        throw StereoException("SegmentJunction::setJunctionPointDisparity", "index is not valid");
    }
    
    int x = junctionPoints_[pointIndex].x();
    int y = junctionPoints_[pointIndex].y();
    junctionPoints_[pointIndex].clear();
    junctionPoints_[pointIndex].set(x, y);
    junctionPoints_[pointIndex].appendDisparity(disparity);
}
