#ifndef REVLIB_DRAWING_H
#define REVLIB_DRAWING_H

#include <vector>
#include "Image.h"
#include "Keypoint.h"

namespace rev {
    
void drawLine(const int startX,
              const int startY,
              const int endX,
              const int endY,
              const Color<unsigned char>& lineColor,
              Image<unsigned char>& image);
void drawCircle(const int centerX,
                const int centerY,
                const int radius,
                const Color<unsigned char>& circleColor,
                Image<unsigned char>& image);
    
Image<unsigned char> drawSegmentBoundary(const Image<unsigned char>& originalImage,
                                         const Image<unsigned short>& segmentImage,
                                         const Color<unsigned char>& boundaryColor = Color<unsigned char>(255, 0, 0));
    
Image<unsigned char> drawKeypoint(const Image<unsigned char>& originalImage,
                                  const std::vector<Keypoint>& keypoints,
                                  const Color<unsigned char>& lineColor = Color<unsigned char>(255, 0, 0));
    
}

#endif
